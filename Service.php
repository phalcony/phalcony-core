<?php

namespace Phalcony\Core;

use Phalcon\DiInterface;

class Service
{
    protected $_di;
    protected $_errors = array();

    public function getErrors()
    {
        return $this->_errors;
    }

    public function getDI()
    {
        return $this->_di;
    }

    public function setDI(DiInterface $di)
    {
        $this->_di = $di;
    }
}
