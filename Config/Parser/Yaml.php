<?php

namespace Phalcony\Core\Config\Parser;

use Composer\Script\Event;
use Phalcon\Config;
use Phalcon\Config\Adapter\Yaml as PYaml;

/**
 * Recursive YAML loader with import features.
 *
 * @author pablo.sanchezs
 */
class Yaml
{
    protected static $rootConfig = null;

    protected static $processedConfigs = array();

    /**
     * Process data recursively and returns a Phalcon Yaml Adapter
     *
     * @param unknown $filePath
     * @param array $callbacks
     * @return Phalcon\Config\Adapter\Yaml
     */
    public static function getConfig($filePath, $callbacks = array())
    {
        if (!static::$rootConfig) {
            static::$rootConfig = $filePath;
        }
        if (!isset(static::$processedConfigs[$filePath])) {
            static::$processedConfigs[$filePath] = new PYaml($filePath, $callbacks);
            static::$processedConfigs[$filePath] = static::recursiveLoad((array) static::$processedConfigs[$filePath], $callbacks);
            if ($filePath == static::$rootConfig) {
                static::$rootConfig                  = null;
                static::$processedConfigs[$filePath] = static::processParameters(
                    static::$processedConfigs[$filePath],
                    static::$processedConfigs[$filePath]['parameters']
                );
                return new Config(static::$processedConfigs[$filePath]);
            } else {
                return (array) static::$processedConfigs[$filePath];
            }
        }
    }

    /**
     * Reads YAML in a recursive way, allowing it to import and overwrite definitions
     * Useful for multiple environments on one install
     *
     * @param  [type] $parsedData [description]
     * @param  array  $callbacks  [description]
     * @return [type]             [description]
     */
    public static function recursiveLoad($parsedData, $callbacks = array())
    {
        if (key_exists('imports', $parsedData)) {
            foreach ($parsedData['imports'] as $config) {
                $extConfig = static::getConfig($config['resource'], $callbacks);
                if ($extConfig) {
                    $parsedData = static::mergeArrayWithLastValueOverOld(
                        static::castRecursivelyToArray($parsedData),
                        static::castRecursivelyToArray($extConfig)
                    );
                }
            }
        }
        return $parsedData;
    }

    /**
     * Merges configs overwriting old values with new ones
     *
     * @param  [type] $arrayOrig    [description]
     * @param  [type] $arrayToMerge [description]
     * @return [type]               [description]
     */
    public static function mergeArrayWithLastValueOverOld($arrayOrig, $arrayToMerge)
    {
        foreach ($arrayToMerge as $key => $value) {
            if (is_array($value)) {
                if (!isset($arrayOrig[$key])) {
                    $arrayOrig[$key] = array();
                }
                $arrayOrig[$key] = static::mergeArrayWithLastValueOverOld($value, $arrayOrig[$key]);
            } else {
                $arrayOrig[$key] = $value;
            }
        }
        return $arrayOrig;
    }

    /**
     * Basically replaces config %VALUES% with the defined parameters
     *
     * @param  [type] $array      [description]
     * @param  [type] $parameters [description]
     * @return [type]             [description]
     */
    public static function processParameters($array, $parameters)
    {
        if (is_array($parameters)) {
            foreach ($array as $key => $value) {
                if (is_array($value)) {
                    $array[$key] = static::processParameters($value, $parameters);
                } else {
                    foreach ($parameters as $pkey => $pval) {
                        $array[$key] = static::setParameter($array[$key], $pkey, $pval);
                    }
                }
            }
        }
        return $array;
    }

    public static function setParameter($currVal, $pkey, $pval)
    {
        if ($currVal == '%' . $pkey . '%') {
            $currVal = $pval;
        }
        return $currVal;
    }

    /**
     * Super power converts any object to an array (multidimension or simple)
     * @param  [type] $object [description]
     * @return [type]         [description]
     */
    public static function castRecursivelyToArray($object)
    {
        return json_decode(json_encode($object), true);
    }

    /**
     *
     */
    public static function cleanYamlQuotes(Event $event)
    {
        $extras = $event->getComposer()->getPackage()->getExtra();

        if (!isset($extras['phalcony-core-yaml'])) {
            throw new \InvalidArgumentException('The parameter handler needs to be configured through the extra.multi-core-yaml setting.');
        }

        $configs = $extras['phalcony-core-yaml'];

        if (!is_array($configs)) {
            throw new \InvalidArgumentException('The extra.multi-core-yaml setting must be an array or a configuration object.');
        }

        if (array_keys($configs) !== range(0, count($configs) - 1)) {
            $configs = array($configs);
        }

        foreach ($configs as $config) {
            if (!is_array($config)) {
                throw new \InvalidArgumentException('The extra.multi-core-yaml setting must be an array of configuration objects.');
            }

            file_put_contents(
                $config['file'],
                str_replace(
                    '\'', '', file_get_contents($config['file'])
                )
            );
        }
    }
}
