<?php

namespace Phalcony\Core\Controller;

use Phalcony\Core\Controller as AbstractController;
use Phalcony\Core\Business\ModelBusiness;

abstract class Rest extends AbstractController
{
    /**
     * Constantes para as informações de paginação
     */
    const PAGESIZE      = 'pageSize';
    const CURRENTPAGE   = 'currentPage';
    const FILTERS       = 'filtros';
    const SORTFIELDS    = 'sortFields';
    const SORTDIR       = 'sortDirections';
    const REPBUSINESS   = 'Business';
    const REPCONTROLLER = 'Controller';
    const BUSINESSCLASS = '\Phalcony\Core\Business\ModelBusiness';

    /**
     * $businessClass Nome da Service para a Controller
     *
     * @var string
     */
    protected $businessClass = null;

    /**
     * Instância da Business da Controller
     * @var \Phalcony\Core\Business
     */
    protected $business = null;

    public function onConstruct()
    {
        $this->getBusiness()->setDI($this->di);
    }

    /**
     * @Route("/", methods={"GET"} )
     */
    public function indexAction($data = null)
    {
        $this->setJsonResponse(
            array('msg' => 'OK')
        );
    }

    /**
     * @Route("/", methods={"POST","PUT"} )
     */
    public function saveAction($data = null)
    {
        $data     = json_decode(json_encode($this->request->getJsonRawBody(), JSON_NUMERIC_CHECK), true);
        $response = $this->getBusiness()->save($data)->toArray();
        $this->setJsonResponse($response);
    }

    /**
     * @Route("/{id:[0-9]+}", methods={"GET"} )
     */
    public function getAction($id = null)
    {
        $data = $this->getBusiness()->getBy('id', $id);
        $this->setJsonResponse($data);
    }

    /**
     * @Route("/filtro", methods={"GET", "POST"} )
     */
    public function filtroAction()
    {
        $dataPost = json_decode(json_encode($this->request->getJsonRawBody(), JSON_NUMERIC_CHECK), true);

        $dataPost[static::PAGESIZE]    = isset($dataPost[static::PAGESIZE]) ? $dataPost[static::PAGESIZE] : null;
        $dataPost[static::CURRENTPAGE] = isset($dataPost[static::CURRENTPAGE]) ? $dataPost[static::CURRENTPAGE] : null;
        $dataPost[static::FILTERS]     = isset($dataPost[static::FILTERS]) ? $dataPost[static::FILTERS] : null;
        $dataPost[static::SORTFIELDS]  = isset($dataPost[static::SORTFIELDS]) ? $dataPost[static::SORTFIELDS] : null;
        $dataPost[static::SORTDIR]     = isset($dataPost[static::SORTDIR]) ? $dataPost[static::SORTDIR] : null;

        $data = $this->getBusiness()->getAll(
            $dataPost[static::PAGESIZE],
            $dataPost[static::CURRENTPAGE],
            $dataPost[static::FILTERS],
            $dataPost[static::SORTFIELDS],
            $dataPost[static::SORTDIR]

        );

        $response['list']              = $data->toArray();
        $response[static::CURRENTPAGE] = $dataPost[static::CURRENTPAGE];
        $response[static::PAGESIZE]    = $dataPost[static::PAGESIZE];
        $response['totalResults']      = $this->getBusiness()->countGetAll($dataPost[static::FILTERS]);
        $response[static::SORTFIELDS]  = $dataPost[static::SORTFIELDS];
        $response[static::SORTDIR]     = $dataPost[static::SORTDIR];

        $this->setJsonResponse($response);
    }

    /**
     * @Route("/{id:[0-9]+}", methods={"DELETE"} )
     */
    public function deleteAction($id)
    {
        $this->getBusiness()->delete($id);
        $this->setJsonResponse(['form' => 'deleted']);
    }

    /**
     * Descobre o nome da Business caso não tenha sido definido
     *
     * @return [type] [description]
     */
    public function getBusinessClass()
    {
        if (is_null($this->businessClass)) {
            $arr                  = explode('\\', get_class($this));
            $arr[count($arr) - 2] = static::REPBUSINESS;
            $arr[count($arr) - 1] = str_replace(static::REPCONTROLLER, static::REPBUSINESS, $arr[count($arr) - 1]);
            $this->businessClass  = implode('\\', $arr);
        }
        return $this->businessClass;
    }

    /**
     * Retorna a Service respectiva da Controller
     *
     * @return \Phalcony\Core\Business\ModelBusiness
     */
    protected function getBusiness()
    {
        $this->businessClass = $this->getBusinessClass();
        if (!class_exists($this->businessClass)) {
            $modelName           = str_replace(static::REPBUSINESS, '', $this->businessClass);
            $this->businessClass = static::BUSINESSCLASS;
            $this->business      = new $this->businessClass;
            $this->business->setModelClass($modelName);
        }

        return $this->business = ($this->business ? $this->business : new $this->businessClass);
    }

}
