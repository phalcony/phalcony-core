<?php

namespace Phalcony\Core\Cache;

use Phalcony\Core\RestApplication;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Events\Event;
use Phalcony\Core\Event\SubscriberInterface\BeforeDispatchLoop;

/**
 * Class Cache\Controller
 *
 * Cria um cache para as annotations dos controllers a fim de melhorar a performance em produção.
 *
 * @package Phalcony\Core
 */
class Controller implements BeforeDispatchLoop
{
    public function beforeDispatchLoop(Event $event, Dispatcher $dispatcher)
    {
        if (RestApplication::$APP_ENV == 'prod') {
/*
            // Parse the annotations in the method currently executed
            $annotations = $this->annotations->getMethod(
                $dispatcher->getControllerClass(),
                $dispatcher->getActiveMethod()
            );

            // Check if the method has an annotation 'Cache'
            if ($annotations->has('Cache')) {

                // The method has the annotation 'Cache'
                $annotation = $annotations->get('Cache');

                // Get the lifetime
                $lifetime = $annotation->getNamedParameter('lifetime');

                $options = array('lifetime' => $lifetime);

                // Check if there is a user defined cache key
                if ($annotation->hasNamedParameter('key')) {
                    $options['key'] = $annotation->getNamedParameter('key');
                }

                // Enable the cache for the current method
                $this->view->cache($options);
            }
*/
        }
    }
}
