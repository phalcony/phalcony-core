<?php

namespace Phalcony\Core\Cache;

use Phalcony\Core\Event\SubscriberInterface\BeforeDispatchLoop;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Events\Event;

/**
 * Class Cache\Module
 *
 * Cria um cache para os módulos a fim de melhorar a performance em produção.
 *
 * @package Phalcony\Core
 */
class Module implements BeforeDispatchLoop
{
    public function beforeDispatchLoop(Event $event, Dispatcher $dispatcher)
    {
        if (RestApplication::$APP_ENV == 'prod') {
        }
    }
}
