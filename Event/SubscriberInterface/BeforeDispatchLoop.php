<?php

namespace Phalcony\Core\Event\SubscriberInterface;

use Phalcon\Mvc\Dispatcher;
use Phalcon\Events\Event;

interface BeforeDispatchLoop
{
    public function beforeDispatchLoop(Event $event, Dispatcher $dispatcher);
}