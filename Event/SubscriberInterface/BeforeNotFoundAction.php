<?php

namespace Phalcony\Core\Event\SubscriberInterface;

use Phalcon\Mvc\Dispatcher;
use Phalcon\Events\Event;

interface BeforeNotFoundAction
{
    public function beforeNotFoundAction(Event $event, Dispatcher $dispatcher);
}