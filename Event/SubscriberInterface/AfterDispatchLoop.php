<?php

namespace Phalcony\Core\Event\SubscriberInterface;

use Phalcon\Mvc\Dispatcher;
use Phalcon\Events\Event;

interface AfterDispatchLoop
{
    public function afterDispatchLoop(Event $event, Dispatcher $dispatcher);
}