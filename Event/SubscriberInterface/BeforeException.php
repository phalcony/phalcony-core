<?php

namespace Phalcony\Core\Event\SubscriberInterface;

use Phalcon\Mvc\Dispatcher;
use Phalcon\Events\Event;

interface BeforeException
{
    public function beforeException(Event $event, Dispatcher $dispatcher);
}