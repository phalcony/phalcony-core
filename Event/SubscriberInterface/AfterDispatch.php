<?php

namespace Phalcony\Core\Event\SubscriberInterface;

use Phalcon\Mvc\Dispatcher;
use Phalcon\Events\Event;

interface AfterDispatch
{
    public function afterDispatch(Event $event, Dispatcher $dispatcher);
}