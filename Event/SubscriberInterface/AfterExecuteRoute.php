<?php

namespace Phalcony\Core\Event\SubscriberInterface;

use Phalcon\Mvc\Dispatcher;
use Phalcon\Events\Event;

interface AfterExecuteRoute
{
    public function afterExecuteRoute(Event $event, Dispatcher $dispatcher);
}