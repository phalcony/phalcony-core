<?php

namespace Phalcony\Core\Event\SubscriberInterface;

use Phalcon\Mvc\Dispatcher;
use Phalcon\Events\Event;

interface BeforeExecuteRoute
{
    public function beforeExecuteRoute(Event $event, Dispatcher $dispatcher);
}