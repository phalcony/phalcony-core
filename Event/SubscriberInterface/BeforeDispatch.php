<?php

namespace Phalcony\Core\Event\SubscriberInterface;

use Phalcon\Mvc\Dispatcher;
use Phalcon\Events\Event;

interface BeforeDispatch
{
    public function beforeDispatch(Event $event, Dispatcher $dispatcher);
}