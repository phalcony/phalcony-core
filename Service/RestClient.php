<?php

namespace Phalcony\Core\Service;

use \Httpful\Request;

/**
 * Implementa um cliente para Rest baseado na biblioteca HTTPFUL
 * Para maiores detalhes sobre o funcionamento da biblioteca, vide
 * http://phphttpclient.com/
 */
abstract class RestClient
{
    protected $_di;
    protected $_errors;

    public function getErrors()
    {
        return $this->_errors;
    }

    public function getDI()
    {
        return $this->_di;
    }

    public function setDI($di)
    {
        $this->_di = $di;
    }

    /**
     * Se houver configuração para proxy,
     * devemos utilizar o mesmo para nos conectar na internet
     *
     */
    public function setProxy(Request $request)
    {
        $config = $this->getDI()->get('config');
        if (isset($config->proxy->host) && $config->proxy->host) {
            $request->useProxy(
                $config->proxy->host,
                $config->proxy->port,
                CURLAUTH_BASIC,
                $config->proxy->auth_username,
                $config->proxy->auth_password
            );
        }
    }

    /**
     * Obtém os dados do serviço rest remoto
     * @param  [type] $uri  [description]
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function getData($uri, $data)
    {
        if (is_array($data)) {
            $data = implode('/', $data);
        }

        $request = Request::get($uri . $data);
        $this->setProxy($request);
        $request->expectsJson();
        $response = $request->send();
        return $response->body;
    }
}
