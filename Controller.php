<?php

namespace Phalcony\Core;

use Phalcon\Mvc\Controller as PhalconController;

abstract class Controller extends PhalconController
{
    /**
     * $service Nome da Service para a Controller
     *
     * @var string
     */
    protected $service = null;

    public function onConstruct()
    {
        $this->getService()->setDI($this->di);
    }

    /**
     * Retorna a Service respectiva da Controller
     *
     * @return Phalcony\Core\Service\ServiceAbstract
     */
    protected function getService()
    {
        if (is_null($this->service)) {
            $arr                  = explode('\\', get_class($this));
            $arr[count($arr) - 2] = 'Service';
            $arr[count($arr) - 1] = str_replace('Controller', '', $arr[count($arr) - 1]);
            $this->service        = implode('\\', $arr);
            $this->service        = new $this->service;
        } else if (is_string($this->service)) {
            $this->service = new $this->service;
        }
        return $this->service;
    }

    /**
     * Define a resposta com saída JSON sempre
     *
     * [serializeData description]
     * @return [type] [description]
     */
    public function setJsonResponse($data)
    {
        if ($data instanceof Model) {
            $data = $data->toArray();
        }

        if (is_array($data)) {
            $data = json_decode(json_encode($data, JSON_NUMERIC_CHECK), true);
        }

        $this->response->setJsonContent($data);
    }
}
