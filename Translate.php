<?php

namespace Phalcony\Core;

use Phalcony\Core\Exception\Translate as TranslateException;

/**
 * Classe que
 */
class Translate extends Service
{
    protected $messages = array();

    public function __construct($di)
    {
        $this->setDI($di);
        $this->addModuleTranslation(__DIR__ . '/Translation');
    }

    /**
     * Obtém a linguagem definida pelo navegador
     *
     * @return [type] [description]
     */
    public function getBestLanguage()
    {
        /** @var string  Idiomas submetidos pelo navegador */
        return $this->getDI()->get('request')->getBestLanguage();
    }

    /**
     * Adiciona as mensagens do módulo ao array de mensagens
     *
     * @param [type] $modulePath [description]
     */
    public function addModuleTranslation($modulePath)
    {
        $transFile = $modulePath . '/' . $this->getBestLanguage() . '.csv';
        if (file_exists($transFile)) {
            $handle = fopen($transFile, 'r');
            while (($data = fgetcsv($handle, 0, "|")) !== false) {
                //Código da mensagem utilizado como índice
                $code = trim($data[0]);
                //Mensagem para o código especificado
                if (isset($this->messages[$code])) {
                    throw new TranslateException("Sobreposição de mensagens ao processar $code em $modulePath", 1);
                }
                $this->messages[$code] = trim($data[1]);
            }
        }
    }

    /**
     * Obtém a mensagem do arquivo de tradução, formatando a saída com os arumentos passados
     * Vide http://php.net/manual/pt_BR/function.vprintf.php
     *
     * @param  [type] $code [description]
     * @param  array  $args [description]
     * @return [type]       [description]
     */
    public function getMessage($code)
    {
        $code = trim($code);
        if (!$code) {
            throw new TranslateException("Nenhum código informado");
        }
        if (isset($this->messages[$code])) {
            return $this->messages[$code];
        }
        return $code;
    }

}
