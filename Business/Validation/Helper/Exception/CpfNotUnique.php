<?php

namespace Phalcony\Core\Business\Validation\Helper\Exception;

use Phalcony\Core\Business\Exception\ModelBusinessException;

class CpfNotUnique extends ModelBusinessException
{
    protected $message = 'VAL::005';
}
