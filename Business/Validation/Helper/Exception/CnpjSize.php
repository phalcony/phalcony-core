<?php

namespace Phalcony\Core\Business\Validation\Helper\Exception;

use Phalcony\Core\Business\Exception\ModelBusinessException;

class CnpjSize extends ModelBusinessException
{
    protected $message = 'VAL::003';
}
