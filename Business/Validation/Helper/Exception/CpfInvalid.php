<?php

namespace Phalcony\Core\Business\Validation\Helper\Exception;

use Phalcony\Core\Business\Exception\ModelBusinessException;

class CpfInvalid extends ModelBusinessException
{
    protected $message = 'VAL::004';
}
