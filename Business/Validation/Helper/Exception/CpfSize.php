<?php

namespace Phalcony\Core\Business\Validation\Helper\Exception;

use Phalcony\Core\Business\Exception\ModelBusinessException;

class CpfSize extends ModelBusinessException
{
    protected $message = 'VAL::006';
}
