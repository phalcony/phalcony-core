<?php

namespace Phalcony\Core\Business\Validation\Helper\Exception;

use Phalcony\Core\Business\Exception\ModelBusinessException;

class CnpjInvalid extends ModelBusinessException
{
    protected $message = 'VAL::001';
}
