<?php

namespace Phalcony\Core\Business\Validation\Helper;

use Phalcony\Core\Business\Validation\Helper\Exception\CnpjInvalid;
use Phalcony\Core\Business\Validation\Helper\Exception\CnpjNotUnique;
use Phalcony\Core\Business\Validation\Helper\Exception\CnpjSize;

abstract class CnpjValidator implements IsValidHelperInterface, IsUniqueHelperInterface
{
    protected static $invalidCpfs = array(
        '00000000000000',
        '11111111111111',
        '22222222222222',
        '33333333333333',
        '44444444444444',
        '55555555555555',
        '66666666666666',
        '77777777777777',
        '88888888888888',
        '99999999999999',
    );
    /**
     * Verifica se o CNPJ é válido matematicamente
     *
     * @param  [type]  $cnpj [description]
     * @return boolean      [description]
     */
    public static function isValid($cnpj)
    {
        //Limpa . e -
        $cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);

        static::isValidSize($cnpj);

        static::isValidValue($cnpj);

        static::isValidDigit($cnpj, 12);

        static::isValidDigit($cnpj, 13);
    }

    /**
     * Checa o comprimento
     *
     * @param  [type]  $cpf [description]
     * @return boolean      [description]
     */
    public static function isValidSize($cnpj)
    {
        // Valida tamanho
        if (strlen($cnpj) != 14) {
            throw new CnpjSize();
        }
    }

    /**
     * Verifica se não é um valor inválido
     *
     *
     * @param  [type]  $cpf [description]
     * @return boolean      [description]
     */
    public static function isValidValue($cnpj)
    {
        // Verifica se nenhuma das sequências invalidas abaixo
        // foi digitada. Caso afirmativo, retorna falso
        if (in_array($cnpj, static::$invalidCpfs)) {
            throw new CnpjInvalid();
        }
    }

    /**
     * Valida o primeiro dígito
     *
     * @param  [type]  $cpf [description]
     * @return boolean      [description]
     */
    public static function isValidDigit($cnpj, $dig)
    {
        // Valida primeiro dígito verificador
        $soma = 0;

        for ($i = 0, $j = 5; $i < $dig; $i++, ($j = ($j == 2) ? 9 : $j - 1)) {
            $soma += $cnpj{$i} * $j;
        }

        $resto = $soma % 11;

        if ($cnpj{$dig} != ($resto < 2 ? 0 : 11 - $resto)) {
            throw new CnpjInvalid();
        }
    }

    /**
     * Verifica o segundo dígito
     *
     * @param  [type]  $cpf [description]
     * @return boolean      [description]
     */
    public static function isValidSecondDigit($cnpj)
    {
        // Valida segundo dígito verificador
        $soma = 0;

        for ($i = 0, $j = 6; $i < 13; $i++, ($j = ($j == 2) ? 9 : $j - 1)) {
            $soma += $cnpj{$i} * $j;
        }

        $resto = $soma % 11;

        if ($cnpj{13} != ($resto < 2 ? 0 : 11 - $resto)) {
            throw new CnpjInvalid();
        }
    }

    /**
     * Verifica se o CNPJ já não existe no banco de dados
     *
     * @param  [type]  $pj [description]
     * @param  [type]  $field [description]
     * @throws Phalcony\Core\Business\Validation\Helper\Exception\CnpjNotUnique
     */
    public static function isUnique($pj, $field = 'cnpj')
    {
        $dbPj = $pj->findFirst(['conditions' => $field . ' = ' . trim($pj->$field)]);
        if ($dbPj && $dbPj->id != $pj->id) {
            throw new CnpjNotUnique();
        }
    }
}
