<?php

namespace Phalcony\Core\Business\Validation\Helper;

interface IsValidHelperInterface
{
    public static function isValid($data);
}
