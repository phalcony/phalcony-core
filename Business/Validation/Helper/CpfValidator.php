<?php

namespace Phalcony\Core\Business\Validation\Helper;

use Phalcony\Core\Business\Validation\Helper\Exception\CpfInvalid;
use Phalcony\Core\Business\Validation\Helper\Exception\CpfNotUnique;
use Phalcony\Core\Business\Validation\Helper\Exception\CpfSize;

abstract class CpfValidator implements IsValidHelperInterface, IsUniqueHelperInterface
{
    protected static $invalidCpfs = array(
        '00000000000',
        '11111111111',
        '22222222222',
        '33333333333',
        '44444444444',
        '55555555555',
        '66666666666',
        '77777777777',
        '88888888888',
        '99999999999',
    );

    /**
     * Verifica se o CPF é válido matematicamente
     *
     * @param  [type]  $cpf [description]
     * @return boolean      [description]
     */
    public static function isValid($cpf)
    {
        //Limpa . e -
        $cpf = (string) str_pad(preg_replace('/[^0-9]/', '', (string) $cpf), 11, '0', STR_PAD_LEFT);

        static::isValidSize($cpf);

        static::isValidValue($cpf);

        static::isValidDigit($cpf, 10, 9);

        static::isValidDigit($cpf, 11, 10);
    }

    /**
     * Checa o comprimento
     *
     * @param  [type]  $cpf [description]
     * @return boolean      [description]
     */
    public static function isValidSize($cpf)
    {
        // Valida tamanho
        if (!strstr($cpf, '-') && strlen($cpf) != 11) {
            throw new CpfInvalid();
        }
        if (strstr($cpf, '-') && strlen($cpf) != 14) {
            throw new CpfSize();
        }
    }

    /**
     * Verifica se não é um valor inválido
     *
     *
     * @param  [type]  $cpf [description]
     * @return boolean      [description]
     */
    public static function isValidValue($cpf)
    {
        // Verifica se nenhuma das sequências invalidas abaixo
        // foi digitada. Caso afirmativo, retorna falso
        if (in_array($cpf, static::$invalidCpfs)) {
            throw new CpfInvalid();
        }
    }

    /**
     * Valida o primeiro dígito
     *
     * @param  [type]  $cpf [description]
     * @return boolean      [description]
     */
    public static function isValidDigit($cpf, $i1, $i2)
    {
        // Calcula e confere primeiro dígito verificador
        $soma = 0;

        for ($i = 0, $j = $i1; $i < $i2; $i++, $j--) {
            $soma += $cpf{$i} * $j;
        }

        $resto = $soma % 11;

        if ($cpf{$i2} != ($resto < 2 ? 0 : 11 - $resto)) {
            throw new CpfInvalid();
        }
    }

    /**
     * Verifica se o CPF já não existe no banco de dados
     *
     * @param  [type]  $pf [description]
     * @param  [type]  $field [description]
     * @throws Phalcony\Core\Business\Validation\Helper\Exception\CpfNotUnique
     */
    public static function isUnique($pf, $field = 'cpf')
    {
        $dbPf = $pf->findFirst(['conditions' => $field . ' = ' . trim($pf->$field)]);
        if ($dbPf && $dbPf->id != $pf->id) {
            throw new CpfNotUnique();
        }
    }

}
