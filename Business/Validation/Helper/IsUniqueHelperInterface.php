<?php

namespace Phalcony\Core\Business\Validation\Helper;

interface IsUniqueHelperInterface
{
    public static function isUnique($object, $field = null);
}
