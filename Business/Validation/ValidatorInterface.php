<?php

namespace Phalcony\Core\Business\Validation;

interface ValidatorInterface
{
    /**
     * Validação antes de persistir
     *
     * @param  [type] $data - Massa de dados a ser validada
     * @throws Phalcony\Core\Business\Exception\ModelBusinessException
     */
    public function saveValidation($data);

    /**
     * Validação antes de excluir
     *
     * @param  [type] $data - Massa de dados a ser validada
     * @throws Phalcony\Core\Business\Exception\ModelBusinessException
     */
    public function deleteValidation($data);
}
