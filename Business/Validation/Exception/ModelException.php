<?php
namespace Phalcony\Core\Business\Validation\Exception;

use Phalcony\Core\Business\Exception;

class ModelException extends \Exception
{
    protected $message = 'VAL::007';
}
