<?php

namespace Phalcony\Core\Business\Exception;

use Phalcony\Core\Business\Exception;

class ModelBusinessException extends Exception
{
    protected $message = 'VAL::007';
}
