<?php

namespace Phalcony\Core;

use Phalcon\Mvc\Model as PhalconModel;
use Phalcon\Mvc\Model\ValidatorInterface;

class Model extends PhalconModel
{
    public function initialize()
    {
        $this->setConnectionService('default');
    }

    public function setDataSource($source)
    {
        parent::setSource($source);
    }

    public function validate(ValidatorInterface $validator)
    {
        parent::validate($validator);
        return $this;
    }
}
