<?php

namespace Phalcony\Core;

use Phalcony\Core\Business\Exception as BusinessException;
use Phalcony\Core\Cache\Controller;
use Phalcony\Core\Config\Parser\Yaml;
use Phalcony\Core\Translate;
use Phalcon\Annotations\Adapter\Files;
use Phalcon\Annotations\Adapter\Memory;
use Phalcon\DiInterface;
use Phalcon\Di\FactoryDefault;
use Phalcon\Events\Event;
use Phalcon\Loader;
use Phalcon\Mvc\Application as PApplication;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Mvc\Router\Annotations as Router;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Session\Adapter\Files as SessionAdapter;

/**
 * CTIS Phalcon Application
 *
 * @author pablo.sanchezs
 */
class RestApplication extends PApplication
{
    /**
     * Dependency Injector for the Application
     *
     * @var  \Phalcon\DiInterface
     */
    protected $_di;

    /**
     * Parser de Yaml
     *
     * @var Phalcony\Core\Config\Parser\Yaml
     */
    protected $_config;

    public static $APP_ENV;
    public static $APP_PATH;
    public static $CONFIG_PATH;
    public static $CONFIG_ENV_PATH;

    /**
     * Sets RestApplication ready to go live and kicking
     *
     * @param string $env - environment (dev, tst, hmg or prod)
     * @param string $path - path relative from index.php to the app folder (default is ..
     */
    public function __construct(
        $env = 'dev',
        $path = '..'
    )
    {
        /**
         * Class statics paths and environment
         */
        self::$APP_ENV = getenv('APP_ENV') ? getenv('APP_ENV') : $env;
        self::$APP_PATH = realpath($path);
        self::$CONFIG_PATH = self::$APP_PATH
            . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR;
        self::$CONFIG_ENV_PATH = self::$CONFIG_PATH . "env" . DIRECTORY_SEPARATOR . self::$APP_ENV;

        /**
         * Register services in DI
         */
        $this->registerDefaultServices();

        /**
         * Register All Modules for the application
         */
        $this->registerVendorsModules();
        $this->registerApplicationModules();

        $modules = $this->getModules();
        foreach ($modules as $moduleName => $module) {
            /**
             * Register services from the vendors and application
             */
            $this->registerServices($module);
            $this->registerBusinessServices($module);

            /**
             * Register All Routes
             */
            $this->registerRoutes($moduleName, $module);

            /**
             * Register module's event listeners
             */
            $this->subscribeEventListeners($module);
        }

        /**
         * Disable template engine
         */
        $this->useImplicitView(false);

        /**
         * Call parent constructor registering DI
         */
        parent::__construct($this->getDI());
    }

    /**
     * Activate CORS headers
     *
     * @return [type] [description]
     */
    public function setCorsHeaders()
    {
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            $this->response->setHeader("Access-Control-Allow-Origin", $_SERVER['HTTP_ORIGIN']);
            $this->response->setHeader('Access-Control-Allow-Credentials', 'true');
            $this->response->setHeader('Access-Control-Max-Age', 86400);
        }
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
                $this->response->setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
            }
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
                $this->response->setHeader("Access-Control-Allow-Headers", $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']);
            }
            $this->response->sendHeaders();
            exit(0);
        }
    }

    /**
     * Handle the request and prepare output for JSON format
     *
     * @param  [type] $uri [description]
     * @return [type]      [description]
     */
    public function handle($uri = null)
    {
        /**
         * Start buffering output to gzip content
         */
        ob_start("ob_gzhandler");

        $this->setCorsHeaders();
        $this->response->setContentType('application/json', 'UTF-8');

        try {
            parent::handle($uri);
            $this->response->setRawHeader('HTTP/1.1 200 OK');
            $this->response->setStatusCode(200, 'OK');
        } catch (\Exception $e) {
            $msg = $this->processException($e);
            $code = $e->getCode() ? $e->getCode() : 400;
            $this->response->setStatusCode($code, "Bad request");
            $this->response->setJsonContent(
                [
                    'erros' => [
                        [
                            'type' => 'danger',
                            'msg' => $this->getDI()->get('translate')->getMessage($msg),
                        ],
                    ],
                ]
            );
        }
        $this->response->send();
    }

    /**
     * Process exceptions. If they are Business exceptions, the exception message is used,
     * otherwise, it shows a generic message
     */
    public function processException($e)
    {
        if (self::$APP_ENV == 'prod') {
            return ($e instanceof BusinessException) ?
                $e->getMessage() :
                'Não foi possível processar a requisição.';
        }
        return $e->getMessage() . '\n' . $e->getTraceAsString();
    }

    /**
     * Set DI for application
     *
     * @param DiInterface $dependencyInjector [description]
     */
    public function setDI(DiInterface $dependencyInjector)
    {
        $this->_di = $dependencyInjector;
    }

    /**
     * Get DI set for application
     *
     * @return [type] [description]
     */
    public function getDI()
    {
        return $this->_di;
    }

    public function getConfig()
    {
        if (!$this->_config) {
            $configFile = self::$CONFIG_PATH . "env" . DIRECTORY_SEPARATOR . self::$APP_ENV . '/config.yml';

            $this->_config = Yaml::getConfig($configFile, array(
                '!app_path' => function ($value) {
                    return self::$APP_PATH . $value;
                },
                '!app_env' => function ($value) {
                    return self::$APP_ENV . $value;
                },
                '!config_env_path' => function ($value) {
                    return self::$CONFIG_ENV_PATH . $value;
                },
            ));
        }

        return $this->_config;
    }

    public function registerDefaultServices()
    {
        /**
         * The FactoryDefault Dependency Injector automatically registers the right services to provide a full stack framework
         */
        $this->_di = new FactoryDefault();

        /**
         * Get configuration
         */
        $config = $this->getConfig();

        /**
         * Metadata Cache
         */
        $this->_di->setShared('modelsMetadata', function () {
            $metaData = new \Phalcon\Mvc\Model\MetaData\Memory(array(
                "lifetime" => 1,
            ));
            return $metaData;
        });

        /**
         * Config as a service
         *
         * @return Phalcon\Config\Adapter\Yaml
         */
        $this->_di->setShared('config', function () use ($config) {
            return $config;
        });

        /**
         * Tradução para multimódulos
         *
         * @return Phalcony\Core\Translate
         */
        $di = $this->_di;

        $this->_di->setShared('translate', function () use ($di) {
            return new Translate($di);
        });

        /**
         * Conexão de banco de dados:
         *
         * @return Phalcon\Db\Adapter\Pdo
         */
        foreach ($config->database as $conn => $dbconfig) {
            $this->_di->set($conn, function () use ($dbconfig) {
                if (!class_exists($dbconfig->adapter)) {
                    $class = '\\Phalcon\\Db\\Adapter\\Pdo\\' . $dbconfig->adapter;
                } else {
                    $class = $dbconfig->adapter;
                }
                return new $class((array)$dbconfig);
            });
        }

        /**
         * Default transaction manager database service
         */
        $this->_di->setShared('transactionManager', function () {
            $tx = new \Phalcon\Mvc\Model\Transaction\Manager();
            $tx->setDbService('default');
            return $tx;
        });

        /**
         * Registering a loader
         *
         * @return Phalcon\Loader
         */
        $this->_di->setShared('loader', function () {
            return new Loader();
        });

        /**
         * Registering a router
         *
         * @return Phalcon\Mvc\Router\Annotations
         */
        $this->_di->setShared('router', function () {
            $router = new Router();
            $router->setDefaultModule('core');
            $router->setDefaultNamespace('Core\Controller');
            return $router;
        });

        /**
         * The URL component is used to generate all kinds of URLs in the application
         *
         * @return Phalcon\Mvc\Url
         */
        $this->_di->setShared('url', function () use ($config) {
            $url = new UrlResolver();
            $url->setBaseUri($config->application->baseUri);
            return $url;
        });

        /**
         * If the configuration specify the use of metadata adapter use it or use memory otherwise
         *
         * @return Phalcon\Mvc\Model\Metadata\Memory
         */
        $this->_di->setShared('modelsMetadata', function () {
            return new MetaDataAdapter();
        });

        /**
         * Starts the session the first time some component requests the session service
         *
         * @return Phalcon\Session\Adapter\Files;
         */
        $this->_di->setShared('session', function () {
            $session = new SessionAdapter();
            $session->start();
            return $session;
        });

        /**
         * Set the default namespace for dispatcher
         *
         * @return Phalcon\Mvc\Dispatcher;
         */
        $this->_di->setShared('dispatcher', function () {
            $dispatcher = new Dispatcher();
            $eventsManager = new \Phalcon\Events\Manager();
            $eventsManager->attach('dispatch', new \Phalcony\Core\Cache\Controller());
            $dispatcher->setEventsManager($eventsManager);
            $dispatcher->setDefaultNamespace('Core\Controller');
            return $dispatcher;
        });

        /**
         * Register the annotation reader
         */
        $this->_di->setShared('annotations', function () {
            $reader = (self::$APP_ENV === 'prod') ?
                new \Phalcon\Annotations\Adapter\Files(['annotationsDir' => self::$APP_PATH
                    . DIRECTORY_SEPARATOR
                    . 'app'
                    . DIRECTORY_SEPARATOR
                    . 'cache'
                    . DIRECTORY_SEPARATOR]) :
                new \Phalcon\Annotations\Adapter\Memory();

            return $reader;
        });

        return $this->_di;
    }

    public function registerModules(array $modulesPaths, $merge = true)
    {
        /**
         * Existing Modules for registration
         * @var array
         */
        $registerModules = [];

        /**
         * Process Modules for Classes Autoload, Routes and Services
         */

        foreach ($modulesPaths as $module) {
            $this->processModule($module, $registerModules);
        }

        parent::registerModules($registerModules, $merge);
    }

    public function processModule($module, &$registerModules)
    {
        /**
         * Autoload Modules
         */
        $loader = $this->getDI()->get('loader');
        $trans = $this->getDI()->get('translate');

        /**
         * Get the Module necessary details
         */

        $modData = $this->getModulePathAndName($module);

        /**
         * All paths for faster autoloading
         */
        $businessDir = $modData['path'] . DIRECTORY_SEPARATOR . 'Business';
        $controllersDir = $modData['path'] . DIRECTORY_SEPARATOR . 'Controller';
        $eventSubscribersDir = $modData['path'] . DIRECTORY_SEPARATOR . 'Event';
        $eventListenersDir = $modData['path'] . DIRECTORY_SEPARATOR . 'Event' . DIRECTORY_SEPARATOR . 'Listener';
        $migrationDir = $modData['path'] . DIRECTORY_SEPARATOR . 'Migration';
        $modelsDir = $modData['path'] . DIRECTORY_SEPARATOR . 'Model';
        $repositoriesDir = $modData['path'] . DIRECTORY_SEPARATOR . 'Repository';
        $servicesDir = $modData['path'] . DIRECTORY_SEPARATOR . 'Service';
        $loader->registerNamespaces([sprintf('%s\Business', $modData['namespace']) => $businessDir], true);
        $loader->registerNamespaces([sprintf('%s\Controller', $modData['namespace']) => $controllersDir], true);
        $loader->registerNamespaces([sprintf('%s\Event', $modData['namespace']) => $eventSubscribersDir], true);
        $loader->registerNamespaces([sprintf('%s\Event\Listener', $modData['namespace']) => $eventListenersDir], true);
        $loader->registerNamespaces([sprintf('%s\Migration', $modData['namespace']) => $migrationDir], true);
        $loader->registerNamespaces([sprintf('%s\Model', $modData['namespace']) => $modelsDir], true);
        $loader->registerNamespaces([sprintf('%s\Repository', $modData['namespace']) => $repositoriesDir], true);
        $loader->registerNamespaces([sprintf('%s\Service', $modData['namespace']) => $servicesDir], true);

        /**
         * Process translations
         */
        $transDir = $modData['path'] . DIRECTORY_SEPARATOR . 'Translation';
        $trans->addModuleTranslation($transDir);

        //Add module to array for a
        $loader->register();

        $registerModules[strtolower($modData['moduleRouterName'])] = [
            'namespace' => $modData['namespace'],
            'className' => sprintf('%s\Module', $modData['namespace']),
            'path' => $modData['path'] . DIRECTORY_SEPARATOR . 'Module.php',
        ];
    }

    public function getModulePathAndName($module)
    {
        $return = [];

        $path = $module;

        $content = file($module . DIRECTORY_SEPARATOR . 'Module.php');
        foreach ($content as $line) {
            if (strstr($line, 'namespace')) {
                $namespace = str_replace(array('namespace', ' ', ';', "\n"), '', $line);
                break;
            }
        }

        $moduleRouterName = explode("\\", $namespace);
        $moduleRouterName = strtolower($moduleRouterName[count($moduleRouterName) - 1]);

        $return['namespace'] = trim($namespace);
        $return['path'] = $path;
        $return['moduleRouterName'] = $moduleRouterName;

        return $return;
    }

    public function getPhalconModuleVendorsPaths()
    {
        /** @var Obtém todas as classes de módulo $m_array */
        $loaders = spl_autoload_functions();
        $loaders[0][0]->getClassMap();
        $m_array = preg_grep('/Module/', $loaders[0][0]->getClassMap());
        $paths = [];
        foreach ($m_array as $module => $path) {
            $interfaces = class_implements($module);
            //O módulo, para ser registra
            if (in_array('Phalcon\\Mvc\\ModuleDefinitionInterface', $interfaces)) {
                $paths[] = dirname($path);
            }
        }

        return $paths;
    }

    /**
     * Find and register modules located on APP_PATH
     */
    public function registerApplicationModules()
    {
        $modulesPaths = glob(self::$APP_PATH
            . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . '*', GLOB_ONLYDIR);
        $this->registerModules($modulesPaths);
    }

    /**
     * Find and register vendors that are Phalcon Modules
     */
    public function registerVendorsModules()
    {
        $modulesPaths = $this->getPhalconModuleVendorsPaths();
        $this->registerModules($modulesPaths);
    }

    /**
     * Register routes for the application
     */
    public function registerRoutes($moduleName, $module)
    {
        $router = $this->getDI()->get("router");

        $controllerDir = str_replace('Module.php', 'Controller', $module['path']);
        if (file_exists($controllerDir)) {
            $controllers = glob($controllerDir . DIRECTORY_SEPARATOR . '*');
            foreach ($controllers as $controller) {
                $controller = explode(DIRECTORY_SEPARATOR, $controller);
                $controllerName = $module['namespace'] . '\\Controller\\' . str_replace('Controller.php', '', $controller[count($controller) - 1]);
                $router->addModuleResource($moduleName, $controllerName);
            }
        }
        $this->getDI()->set("router", $router);
    }

    public function subscribeEventListeners($module)
    {
        $subscriberClass = $module['namespace'] . '\\Event\\Subscriber';
        if (class_exists($subscriberClass)) {
            $subscriber = new $subscriberClass;
            $methods = get_class_methods($subscriber);
            foreach ($methods as $method) {
                $eventsManager = $this->getDI()->get('dispatcher')->getEventsManager();
                $eventsManager->attach('dispatch', $subscriber);
            }
        }
    }

    public function registerServices($module)
    {
        $di = $this->getDI();
        $servicesDir = str_replace('Module.php', 'Service', $module['path']);
        $services = glob($servicesDir . DIRECTORY_SEPARATOR . '*');
        foreach ($services as $service) {
            $this->registerAnnotatedService($service, $module, 'Service');
        }
    }

    public function registerBusinessServices($module)
    {
        $di = $this->getDI();
        $servicesDir = str_replace('Module.php', 'Business', $module['path']);
        $services = glob($servicesDir . DIRECTORY_SEPARATOR . '*');
        foreach ($services as $service) {
            $this->registerAnnotatedService($service, $module, 'Business');
        }
    }

    public function registerAnnotatedService($service, $module, $nsp)
    {
        if (is_file($service)) {
            $service = explode(DIRECTORY_SEPARATOR, $service);
            $service = $module['namespace'] . '\\' . $nsp . '\\' . str_replace('.php', '', $service[count($service) - 1]);
            $reader = $this->getDI()->get('annotations');
            // Reflect the annotations in the class Example
            $reflector = $reader->get($service);

            // Read the annotations in the class' docblock
            $annotations = $reflector->getClassAnnotations();

            if ($annotations) {
                // Traverse the annotations
                foreach ($annotations as $annotation) {
                    // Print the annotation name
                    if ($annotation->getName() == 'Service') {
                        $serviceName = count($annotation->getArguments()) ?
                            $annotation->getArguments()[0] :
                            //If it's marked as service but no name was given, will be registered a
                            //name.space.service
                            str_replace('\\', '.', strtolower($service));
                    }
                }
            } else {
                //Not marked as service will be auto registered as name.space.service
                $serviceName = str_replace('\\', '.', strtolower($service));
            }

            $di = $this->getDI();
            $di->setShared($serviceName, function () use ($di, $service) {
                $service = new $service();
                $service->setDI($di);
                return $service;
            });
        }
    }
}
